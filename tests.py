from slipshot import SlipShot, SlipSection
from uuid import uuid4

@SlipShot("zero0", ['tag'])
def t():
    INFO("Uh Oh1")
    REQUIREEQ(0,0)


@SlipShot("zero1", ['tag'])
def t():
    INFO("Uh Oh")
    REQUIREEQ(0,0)
    REQUIREEQ(0,1)


@SlipShot("Test with two pass and one fail")
def t():
    @SlipSection("B1")
    def t():
        @SlipSection("C1")
        def t():
            REQUIREEQ(0,0)
            @SlipSection("D1")
            def t():
                REQUIREEQ(0,0)
        @SlipSection("C1")
        def t():
            pass

    @SlipSection("B2")
    def t():
        pass

    @SlipSection("B3")
    def t():
        REQUIREEQ(1,2)

@SlipShot("Test with no asserts but warnings")
def t():
    WARN("A")
    @SlipSection("B1")
    def t():
        WARN("B1")
        @SlipSection("C1")
        def t():
            WARN("C1")
            @SlipSection("D1")
            def t():
                WARN("D1")
        @SlipSection("C1")
        def t():
            WARN("C2")

    @SlipSection("B2")
    def t():
        WARN("B2")

    @SlipSection("B3")
    def t():
        WARN("B3")

@SlipShot("Tagged inner section")
def t():
    @SlipSection("B1")
    def t():
        WARN("B1")
        @SlipSection("C1")
        def t():
            WARN("C1")
            @SlipSection("D1", ['inner'])
            def t():
                WARN("D1")

        @SlipSection("C1", ['inner'])
        def t():
            WARN("C2")


@SlipShot("zero2", ['tag'])
def t():
    WARN("DO I RUN?!?!?")
    INFO("Uh Oh PLEASE")
    REQUIREEQ(0,0)
    REQUIREEQ(0,1)

@SlipShot("Primary Subsystem", ['Primary', 'x/y']) #Name and tags
def test():
    #code to get result
    INFO ("I'm about to test two values %s" % 4)

    #STEP('testing y component', 34) #Test description, expected value
    #code to get result
    #ERROR(4) #Raises error on failure.

    INFO("A")

    myUUID = uuid4()
    REQUIREEQ(1,1)

    @SlipSection('B1')
    def sect():
        INFO("B1")

        @SlipSection('C1')
        def sect():
            INFO("C1")

        @SlipSection('C2')
        def sect():
            INFO("C2")
            #REQUIREEQ(4,5)
            WARN(myUUID)

    @SlipSection('B2')
    def sect():
        INFO("B2")
        REQUIREEQ(4,5)

    @SlipSection('B3')
    def sect():
        INFO("B3")
        REQUIREEQ(1,2)

    #Correct output would be A B1 C1, A B1 C2, A B2, A B3

SlipShot()
