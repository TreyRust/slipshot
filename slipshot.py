#The clarity of code is inversely proportional the clarity of the output.
import sys
from termcolor import cprint, colored


#Okay, this was hobbled together in four days
#because I was spoiled by the C++ testing framework "Catch".
#This module is heavily inspired by it.

#But the code is ugly, I know it is. If an ugly stick factory exploded
#into an orphanage, it's shear ugliness wouldn't even compare.

#I'm proud of the fact that it works, but I'm not proud of the code quality
#and lack of commenting.

#But it's functional and it's what I need to get back to work quickly.
#I've spent two days (Well, two half days) on this and I need to get back
#to the project I was needing to test before I started this.


def listInList(list1, list2):
    for i in list1:
        if i in list2:
            return True

    return False

class _SlipContainer_:
    pass

class _SlipShot_:


    #[[[test, testname, testtags], ...], ...]
    tests = []

    #This is where tests that have been run are stored. This is to avoid
    #running the same test twice in the event that there are two copies in
    #tests
    ranTests = []

    gotSections = []
    doneSections = []
    sweeping = False
    infoBuffer = []

    #This is to count the number of Asserts that have been run in
    #the current leaf.
    assertCount = 0

    totalPassed = 0
    totalFailed = 0

    executedTests = []

    warningBuffer = []

    runningTags = []

    #This is the container used to maintain state in the tests.
    #It is injected as 'ATR'
    attributeContainer = _SlipContainer_()

    @classmethod
    def begin(cls):
        print "#" * 80

        for t in sys.argv[1:]:
            cls.runningTags.append(t)

        if cls.runningTags:
            if len(cls.runningTags) != 1:
                sys.stdout.write(colored('Running tests with tags: ', 'cyan'))
            else:
                sys.stdout.write(colored('Running tests with tag: ', 'cyan'))
            for t in cls.runningTags:
                sys.stdout.write(t)
                if not t == cls.runningTags[-1]:
                    sys.stdout.write(', ')

            sys.stdout.write('\n')
            print '=' * 80
        else:
            cprint('Running All Tests.', 'cyan')


        tests = cls._sweepMaster()
        cls._executeTestListWithOutput(tests)

    @classmethod
    def _addTest(cls, test, testname, testtags):

        #Tests are stored in the dict as tag:[[test, name, tags], ...]
        cls.tests.append([test, testname, testtags])


    @classmethod
    def _sweepMaster(cls):
        #Since all the functions have to be run in order to get the
        #nested decorators to fire, we need something to prevent
        #output, asserts from failing before we're ready, the sky from
        #falling and hell from freezing over.
        cls.sweeping = True
        sweptList = []
        for test, name, tags in cls.tests:
            #Perform the sweeping operation.
            cleaned = cls._sweepSections([test, name, [], tags, []])
            sweptList.append(cleaned)


        #Since while sweeping, ATR had to be set to prevent a NameError,
        #we need to reset the container because of all the garbage it
        #might contain now.
        #It is also reset in _doVoodoo but if there are no sections in
        #the main test, then that code never runs anyway.
        cls.attributeContainer = _SlipContainer_()
        #Now we're done sweeping.
        cls.sweeping = False

        #Now remove all that don't have the tag.

        return sweptList



    @classmethod
    def _executeTestListWithOutput(cls, tests):
        ranTests = []
        failedTests = []


        for test, name, parents, tags, sections in tests:
            #Skip tests that have run. Needed because of duplicates in
            #the cls.tests dictionary
            #QUESTION Is it really needed? #Yes I test
            if test in ranTests:
                continue

            #Try/except to catch exceptions raised by the custom assert macros
            try:
                #RUN THAT SUCKA
                cls._doVoodoo([test, name, parents, tags, sections])
            except ValueError as msg:
                print colored(' ..  FAIL', 'red')

                #Dump the info buffer
                if cls.infoBuffer:
                    sys.stdout.write('\n')
                for m in cls.infoBuffer:
                    cprint("INFO: %s" % m, 'white')

                space = ""

                #Name the parent sections with increasing indentation.
                #The first section is the root, which is just the test itself
                #and it's a given that we're in it. So skip it with [1:].
                print colored(msg, 'red')

                failedTests.append(test)

            ranTests.append(test)


        print '=' * 80
        nTests = (len(ranTests))
        fTests = (len(failedTests))
        if failedTests:
            cprint('%s out of %s tests failed' % (fTests, nTests), 'red')
        else:
            cprint('All (%s) tests passed' % cls.totalPassed, 'green')

    @classmethod
    def _sweepSections(cls, testList):
        #[test, name, [[ParentTest, ParentName],...], [Tags], [THIS,...]]
        testFun = testList[0]
        testName = testList[1]
        testParents = testList[2]
        testTags = testList[3]
        testSections = testList[4]

        #Okay, we have to run all the test functions in order
        #to get the nested decorators to fire and capture
        #the sections.

        #Each pass this needs to be reset.
        cls.gotSections = []

        #Prevent NameError, since this is basically a dry run
        testFun.func_globals['ATR'] = cls.attributeContainer

        #This causes the decorators to fire.
        testFun()

        #Iterate over the sections that we got from the SlipSection() decorator.
        for test, name, tags in cls.gotSections:
            #It's imperative that we create a new list, because it's current
            #value applies only to this section and it will change.
            parents = testParents[:]


            #Add this current section to the parental chain
            parents.append([testFun, testName])

            #We need to create a new list, otherwise it will pollute
            tags = tags[:]
            for t in testTags:
                tags.append(t)

            #Run it for our children
            childSections = cls._sweepSections([test, name, parents, tags, []])
            testSections.append(childSections)

        #So, now that we've added child sections and parental chains,
        #let's return the modified list
        return testList


    #This method started out ugly but it was just a late bloomer.
    #Named _doVoodoo because it was going to do some crazy shit I realized
    #later that you can't do in python. Poo.
    @classmethod
    def _doVoodoo(cls, testList):
        #[test, name, [[ParentTest, ParentName],...], [Tags], [THIS,...]]
        testFun = testList[0]
        testName = testList[1]
        testParents = testList[2]
        testTags = testList[3]
        testSections = testList[4]

        #If it has leafs, iterate over them
        for t in testSections:
            #If this leaf hasn't been run before
            if not t in cls.doneSections:
                #Iterate over it's leafs (See above for magic numbers)
                for s in t[4]:
                    #If any of them have been run before, then break, we're done
                    if s in cls.doneSections: #Yo dawg
                        break
                else: #Otherwise, let's pass it deeper.
                    #Since at this point, we're restarting, we reset.
                    cls._resetForTestExecution()

                    #We must go deeper
                    cls._doVoodoo(t) #Who do? You do!


        #If it has no leafs (Let's call it a 'Concrete' section)
        if not testSections:
            if cls.runningTags:
                if not listInList(cls.runningTags, testTags):
                    cls.doneSections.append(testFun)
                    return

            cls._resetForTestExecution()
            #Execute it's parents in order, and make it watch
            #Conveniently doesn't run if there are no children
            space = ''
            for f, n in testParents:
                isRoot = False
                #If we're working on the root function (Only for this loop)
                #then we say so.
                if f == testParents[0][0]:
                    isRoot = True

                #Execute the test
                cls._executeSingleTest(f, n, isRoot, space)

                space = space + ' ' #SPACE IS EQUAL TO SPACE PLUS SPACE


            isRoot = False
            if not testParents:
                print '=' * 80
                isRoot = True

            #Now execute the concrete function.
            cls._executeSingleTest(testFun, testName, depth = space, root = isRoot, concrete = True)


            #Once it has been executed, it's done and shouldn't be run again,
            #because it's a concrete section, remember?
            cls.doneSections.append(testFun)

    @classmethod
    def _resetForTestExecution(cls):
        cls.infoBuffer = []
        cls.attributeContainer = _SlipContainer_()

    @classmethod
    def _executeSingleTest(cls, test, name, root = False, depth = '', concrete = False):
        #This way it's cutely nested :>
        #This is purely for printing the pretty nested sections.
        if not test in cls.executedTests:
            name = colored(name, 'white')
            sys.stdout.write(depth + name)
            if not concrete:
                #If it's not concrete, then it has children that
                #Are about to be displayed, so we add the colon.
                #The space is in case it is also a test.
                sys.stdout.write(": ")
            else:
                #This is to pad the pass or failure messages.
                sys.stdout.write(' ')


        #Inject the ATR variable so that the user can maintain state.
        test.func_globals['ATR'] = cls.attributeContainer

        #This is where we reset variables needed on a per pass basis
        cls.assertCount = 0

        #We clear the warning buffer here because it will fill up
        #after child section of a test that has a warning, but we only
        #want the warning once.
        cls.warningBuffer = []
        #RELEASE THE HOUNDS
        test()


        if not test in cls.executedTests:
            #Now of course, issue any warningBuffer accumulated. But only
            #if we haven't before (That's why it's in this if block)
            for w in cls.warningBuffer:
                sys.stdout.write(colored(' .. WARNING: %s' % w, 'yellow'))

            #If there's no assert, there was no test
            if cls.assertCount != 0:
                sys.stdout.write(colored(' .. PASS', 'green'))
                cls.totalPassed += 1

            #Now it's time for the newline.
            sys.stdout.write('\n')


        cls.executedTests.append(test)



    #The following methods are what get injected into the test environment.
    #wrapped by SlipShot()
    @staticmethod
    def wraprequireeq(v1, v2):
        #This just means that it's currently sweeping and since we might raise
        #an exception (In part do to this being a sweep), we shouldn't run.
        if _SlipShot_.sweeping:
            return

        #Increment the assert counter
        #TODO: Make sure this is tested in future asserts
        _SlipShot_.assertCount += 1
        if v1 == v2:
            #if verbose keep saying shit #There is no 'verbose', dipshit!
            #TODO
            #Not to be confused with a passing case.
            #I haven't marred the language that badly.
            pass
        else:
            #Create the failure message, capturing the values and then raising
            #an exception that the runAllTests method will catch.
            assNum = _SlipShot_.assertCount
            fail = "FAILURE in assert %s REQUIREEQ(%s == %s)" % (assNum, v1, v2)
            raise ValueError(fail)

    @staticmethod
    def wrapinfo(msg):
        if _SlipShot_.sweeping:
            return

        _SlipShot_.infoBuffer.append(msg)

    @staticmethod
    def wrapwarn(msg):
        if _SlipShot_.sweeping:
            return

        _SlipShot_.warningBuffer.append(msg)


#Main decorator
def SlipShot(testname = None, testtags = []):
    #Import this stuff because it won't be there when
    #all the user does is import this function, I know.
    from slipshot import _SlipShot_

    #This means we're being called to boot everything up.
    if testname == None:
        _SlipShot_.begin()

    def decorator(thetest): #Yo dawg...
        #Inject variables into the test environment
        thetest.func_globals['REQUIREEQ'] = _SlipShot_.wraprequireeq
        thetest.func_globals['INFO'] = _SlipShot_.wrapinfo
        thetest.func_globals['WARN'] = _SlipShot_.wrapwarn
        _SlipShot_._addTest(thetest, testname, testtags)
    #Return trash so that the interpreter doesn't yell at us.
    return decorator

def SlipSection(name, tags = []):
    #Import this stuff because it won't be there when
    #all the user does is import this function, I know.
    from slipshot import _SlipShot_
    def dec(func):
        if not func in _SlipShot_.gotSections:
            _SlipShot_.gotSections.append([func, name, tags])
    #Return trash so that the interpreter doesn't yell at us.
    return dec

#A slip segment is just a way to break up a parent test, whether that be a
#Section or main test. They are NOT concrete or leaf tests, they should be run
#Like any other code in a given test, in order, without recursive parent
#calling.
def SlipSegment(name, tags = []):
    #Import this stuff because it won't be there when
    #all the user does is import this function, I know.
    from slipshot import _SlipShot_
    def dec(func):
        if not func in _SlipShot_.gotSections:
            _SlipShot_.gotSections.append([func, name, tags])
    #Return trash so that the interpreter doesn't yell at us.
    return dec
