## What is SlipShot? ##

SlipShot is a Python testing framework heavily inspired by the C++ testing framework 'Catch' by PhilSquared.


## A bare-bones walk-through ##

Let's write a couple of tests.

```
#!python

from slipshot import SlipShot, SlipSection

@SlipShot("My first test!")
def t():
 #Teardown
 #Setup
 
 @SlipSection('First inner test')
 def t():
  #some code
  REQUIREEQ(1,1)
 @SlipSection('Second inner test')
 def t():
  REQUIREEQ(1,2)

SlipShot() #This makes all the tests run.

```


The function handle doesn't matter, so I just call all of them t(). Note that the outermost test uses the @SlipShot decorator but the inner tests use @SlipSection, this is important.

The inner most tests (Those that don't have tests nested inside of them) are considered 'concrete' tests. The tests are run by finding the concrete tests and then all of the tests above them are run (The outermost test is run first and so on until we get to the inner-most) up to that concrete test and then it moves on to the next concrete test.

It's easier to understand with an animated gif

![gif1.gif](https://bitbucket.org/repo/prbRza/images/1338188911-gif1.gif)

And they can be nested arbitrarily deep. Checkout 'tests.py' to see a better demo.

####The results####
![Screenshot.png](https://bitbucket.org/repo/prbRza/images/1181821842-Screenshot.png)

## Handling Output ##
SlipShot has two constructs for output. INFO and WARN.

### WARN ###
WARN is used for writing messages out from the test that will always be printed when they're fired.

```
#!python

@SlipShot("Test")
def t():
 WARN("This is a warning!")
```

![Screenshot-4.png](https://bitbucket.org/repo/prbRza/images/1249195311-Screenshot-4.png)


Since parent tests can be run multiple times, each WARN call only gets printed once, even if it gets called with different values. This may change in the future. The problem is the structuring of the output with deep nesting of tests.

### INFO ###
Info is meant for logging information that is only relevant in the event of a test failure.

```
#!python

@SlipShot("Test")
def t():
 INFO("This is information!!")
 REQUIREEQ(1,2)
```

![Screenshot-5.png](https://bitbucket.org/repo/prbRza/images/2014160348-Screenshot-5.png)


## Issues ##
The worst thing about it is the fact that all the tests are run twice. This is to get the decorators to fire, because decorators within functions won't execute unless the function has been loaded.

I was going to merge the sweep and execute stages, so that it would only run stuff as needed but it would be a half measure when you bring tags into the equation and I don't like half measures.

Although one could argue that tags are pointless anyway when all the code is run at least once regardless of tags. I've left them in just to clarify the output.

The double running problem is icky to me, so I'm probably not going to continue working on SlipShot unless people show interest in it. I came up with what I think is a better idea for a testing framework anyway and it doesn't have these problems.

## How do I do? ##

To play with the demo, which is a lot more impressive than the above, just clone the repository and run 'tests.py'. It's really just a library, so if you want to use it, just make sure slipshot.py is within your application's python path.

To run only tagged tests, just pass in the tag name as an argument.

```
#!bash
python tests.py taghere

```